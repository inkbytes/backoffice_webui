FROM node:latest as build-env
WORKDIR /app

ADD package.json .

RUN npm install --force

ADD . .

RUN npm run build --prod

FROM nginx:alpine

COPY --from=build-env /app/dist /usr/share/nginx/html
COPY nginx/server_nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
