import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AsyncPipe, CommonModule, NgClass, NgFor, NgIf } from '@angular/common';
import { Subscription } from 'rxjs';
import { ChangeDetectionStrategy,ChangeDetectorRef } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { WebsocketService } from '@apis/websocket.service';
import { SafeHtmlPipe } from "@common/pipes/safe-html-pipe.pipe";
import { environment as env} from '@env/environment';
@Component({
    selector: 'logs-viewer',
    standalone: true,
    templateUrl: './viewer.component.html',
    styleUrls: ['./viewer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [CdkVirtualScrollViewport, SafeHtmlPipe,CommonModule]
})
export class ViewerComponent implements OnInit, OnDestroy {
  logMessages: string[] = [];
  connected:  boolean=false;
  private messageSubscription: Subscription=    new Subscription();
  itemSize = 200;
  @ViewChild('logContainer')
    private logContainer!: ElementRef;


  constructor(public websocketService: WebsocketService,
    private cdr: ChangeDetectorRef ) {

  }

  ngOnInit(): void {
    this.websocketService.connect(env.webSocketUrl+"?api_key="+env.webSocketApiKey);
    this.messageSubscription = this.websocketService.messages.subscribe((message: string) => {
        this.logMessages.push(message)
        this.cdr.markForCheck();
        setTimeout(() => {
        ///console.log(message)
        this.scrollToBottom();
         // Manually mark for check
      }, 10000);
    });


  }

  ngOnDestroy(): void {
    this.messageSubscription.unsubscribe();
    this.websocketService.disconnect();
  }

  private scrollToBottom(): void {
    try {
      this.logContainer.nativeElement.scrollTop = this.logContainer.nativeElement.scrollHeight;
    } catch(err) { }
  }
  public startScrape(): void {
  this.websocketService.sendMessage('start_scrape');
}

}
