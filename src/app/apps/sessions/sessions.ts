import { Component, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { animate, style, transition, trigger } from '@angular/animations';
import { ModalComponent } from 'angular-custom-modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
    moduleId: module.id,
    templateUrl: './sessions.html',
    animations: [
        trigger('toggleAnimation', [
            transition(':enter', [style({ opacity: 0, transform: 'scale(0.95)' }), animate('100ms ease-out', style({ opacity: 1, transform: 'scale(1)' }))]),
            transition(':leave', [animate('75ms', style({ opacity: 0, transform: 'scale(0.95)' }))]),
        ]),
    ],
})
export class SessionsComponent {
    constructor(public fb: FormBuilder) {}
    @ViewChild('addTaskModal') addTaskModal!: ModalComponent;
    @ViewChild('viewTaskModal') viewTaskModal!: ModalComponent;
    defaultParams = {
        id: null,
        title: '',
        description: '',
        descriptionText: '',
        assignee: '',
        path: '',
        tag: '',
        priority: 'low',
    };

    selectedTab = '';
    isShowTaskMenu = false;

    params!: FormGroup;
    allTasks = [
     {
        id: 1,
        title: "System Warning - Critical Update",
        date: "Feb, 18 2024",
        description: "⚠️ Urgent system warning! A critical update is scheduled for today at 4:50 pm. Ensure all processes are backed up before the update to avoid disruptions.",
        descriptionText: "⚠️ Urgent system warning! A critical update is scheduled for today at 4:50 pm. Ensure all processes are backed up before the update to avoid disruptions.",
        tag: "system-update",
        priority: "high",
        assignee: "Admin",
        path: "",
        status: "scheduled"
    },
    {
        id: 2,
        title: "System Info - Team Collaboration Meeting",
        date: "Feb, 17 2024",
        description: "ℹ️ System information: Team collaboration meeting held at Starbucks to discuss ongoing projects and upcoming milestones.",
        descriptionText: "ℹ️ System information: Team collaboration meeting held at Starbucks to discuss ongoing projects and upcoming milestones.",
        tag: "team-meeting",
        priority: "medium",
        assignee: "Project Manager",
        path: "team-meeting-17.jpeg",
        status: "completed"
    },
    {
        id: 3,
        title: "System Info - Project Update Discussion",
        date: "Feb, 16 2024",
        description: "ℹ️ System information: Meeting with Lisa to discuss project details and provide updates on the latest developments.",
        descriptionText: "ℹ️ System information: Meeting with Lisa to discuss project details and provide updates on the latest developments.",
        tag: "project-update",
        priority: "high",
        assignee: "John Smith",
        path: "profile-1.jpeg",
        status: "in-progress"
    },
    {
        id: 4,
        title: "System Info - Data Backup Completed",
        date: "Feb, 15 2024",
        description: "ℹ️ System information: Successful completion of data backup process. Ensure data integrity and accessibility.",
        descriptionText: "ℹ️ System information: Successful completion of data backup process. Ensure data integrity and accessibility.",
        tag: "data-backup",
        priority: "low",
        assignee: "IT Administrator",
        path: "backup-success-15.jpeg",
        status: "completed"
    },
  {
    id: 5,
    title: 'System Warning - Critical Update',
    date: 'Feb, 18 2024',
    description:
        '⚠️ Urgent system warning! A critical update is scheduled for today at 4:50 pm. Ensure all processes are backed up before the update to avoid disruptions.',
    descriptionText:
        '⚠️ Urgent system warning! A critical update is scheduled for today at 4:50 pm. Ensure all processes are backed up before the update to avoid disruptions.',
    tag: 'system-update',
    priority: 'high',
    assignee: 'Admin',
    path: '',
    status: 'scheduled'
},
{
    id: 6,
    title: 'System Info - Team Collaboration Meeting',
    date: 'Feb, 17 2024',
    description:
        'ℹ️ System information: Team collaboration meeting held at Starbucks to discuss ongoing projects and upcoming milestones.',
    descriptionText:
        'ℹ️ System information: Team collaboration meeting held at Starbucks to discuss ongoing projects and upcoming milestones.',
    tag: 'team-meeting',
    priority: 'medium',
    assignee: 'Project Manager',
    path: 'team-meeting-17.jpeg',
    status: 'completed'
},
{
    id: 7,
    title: 'System Info - Project Update Discussion',
    date: 'Feb, 16 2024',
    description:
        'ℹ️ System information: Meeting with Lisa to discuss project details and provide updates on the latest developments.',
    descriptionText:
        'ℹ️ System information: Meeting with Lisa to discuss project details and provide updates on the latest developments.',
    tag: 'project-update',
    priority: 'high',
    assignee: 'John Smith',
    path: 'profile-1.jpeg',
    status: 'in-progress'
},
{
    id: 8,
    title: 'System Info - Data Backup Completed',
    date: 'Feb, 15 2024',
    description:
        'ℹ️ System information: Successful completion of data backup process. Ensure data integrity and accessibility.',
    descriptionText:
        'ℹ️ System information: Successful completion of data backup process. Ensure data integrity and accessibility.',
    tag: 'data-backup',
    priority: 'low',
    assignee: 'IT Administrator',
    path: 'backup-success-15.jpeg',
    status: 'completed'
},
{
    id: 9,
    title: 'Conference call with Marketing Manager',
    date: 'Feb, 14 2024',
    description:
        '⚠️ Warning! Ensure all aspects are checked before the conference call with the Marketing Manager. The call is of high priority.',
    descriptionText:
        '⚠️ Warning! Ensure all aspects are checked before the conference call with the Marketing Manager. The call is of high priority.',
    tag: 'update',
    priority: 'high',
    assignee: 'John Smith',
    path: 'profile-5.jpeg',
    status: 'important',
},
{
    id: 10,
    title: 'New User Registered',
    date: 'Feb, 13 2024',
    description:
        '⚠️ Warning! A new user has registered. Check and verify the registration details. This is important for security reasons.',
    descriptionText:
        '⚠️ Warning! A new user has registered. Check and verify the registration details. This is important for security reasons.',
    tag: '',
    priority: 'medium',
    assignee: '',
    path: '',
    status: 'important',
},
{
    id: 11,
    title: 'Fix issues in new project',
    date: 'Feb, 12 2024',
    description:
        'ℹ️ System information: Fix issues in the new project assigned to John Smith. The task is of medium priority.',
    descriptionText:
        'ℹ️ System information: Fix issues in the new project assigned to John Smith. The task is of medium priority.',
    tag: 'team',
    priority: 'medium',
    assignee: 'John Smith',
    path: 'profile-17.jpeg',
    status: '',
},
{
    id: 12,
    title: 'Check All functionality',
    date: 'Feb, 11 2024',
    description:
        '⚠️ Warning! Check all functionalities of the system. This is important for ensuring smooth operations.',
    descriptionText:
        '⚠️ Warning! Check all functionalities of the system. This is important for ensuring smooth operations.',
    tag: 'update',
    priority: 'medium',
    assignee: 'John Smith',
    path: 'profile-18.jpeg',
    status: 'important',
},
{
    id: 13,
    title: 'Check Repository',
    date: 'Feb, 10 2024',
    description:
        'ℹ️ System information: Check the repository for any inconsistencies. The task is of medium priority.',
    descriptionText:
        'ℹ️ System information: Check the repository for any inconsistencies. The task is of medium priority.',
    tag: 'team',
    priority: 'medium',
    assignee: 'John Smith',
    path: 'profile-20.jpeg',
    status: 'complete',
},
{
    id: 14,
    title: 'Trashed Tasks',
    date: 'Feb, 9 2024',
    description:
        '⚠️ Warning! These tasks have been trashed. Review them and ensure no critical information is lost. The tasks are marked as important.',
    descriptionText:
        '⚠️ Warning! These tasks have been trashed. Review them and ensure no critical information is lost. The tasks are marked as important.',
    tag: 'team',
    priority: 'medium',
    assignee: 'John Smith',
    path: 'profile-15.jpeg',
    status: 'trash',
},
{
    id: 15,
    title: 'Trashed Tasks 2',
    date: 'Feb, 8 2024',
    description:
        '⚠️ Warning! These tasks have been trashed. Review them and ensure no critical information is lost. The tasks are marked as important.',
    descriptionText:
        '⚠️ Warning! These tasks have been trashed. Review them and ensure no critical information is lost. The tasks are marked as important.',
    tag: '',
    priority: 'medium',
    assignee: 'John Smith',
    path: 'profile-2.jpeg',
    status: 'trash',
},
        {
            id: 12,
            title: 'Trashed Tasks 3',
            date: 'Aug, 10 2020',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: 'team',
            priority: 'medium',
            assignee: 'John Smith',
            path: 'profile-24.jpeg',
            status: 'trash',
        },
        {
            id: 13,
            title: 'Do something nice for someone I care about',
            date: 'Sep, 10 2022',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-25.jpeg',
            status: '',
        },
        {
            id: 14,
            title: 'Memorize the fifty states and their capitals',
            date: 'Sep, 13 2020',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-11.jpeg',
            status: '',
        },
        {
            id: 15,
            title: 'Watch a classic movie',
            date: 'Oct, 10 2020',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-10.jpeg',
            status: '',
        },
        {
            id: 16,
            title: 'Contribute code or a monetary donation to an open-source software project',
            date: 'Nov, 10 2017',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-12.jpeg',
            status: '',
        },
        {
            id: 17,
            title: 'Solve a Rubik`s cube',
            date: 'Nov, 15 2017',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-25.jpeg',
            status: '',
        },
       {
        id: 12,
        title: "Trashed Tasks 3",
        date: "Aug, 10 2020",
        description:
            "Scraping process warning: Trashed Tasks 3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.",
        descriptionText:
            "Scraping process info: Trashed Tasks 3 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.",
        tag: "team",
        priority: "medium",
        assignee: "John Smith",
        path: "profile-24.jpeg",
        status: "trash",
    },
    {
        id: 13,
        title: "Do something nice for someone I care about",
        date: "Sep, 10 2022",
        description:
            "Scraping process error: Do something nice for someone I care about - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.",
        descriptionText:
            "Scraping process error: Do something nice for someone I care about - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.",
        tag: "",
        priority: "",
        assignee: "John Smith",
        path: "profile-25.jpeg",
        status: "",
    },
        {
            id: 22,
            title: 'Have a football scrimmage with some friends',
            date: 'Sep, 13 2019',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-24.jpeg',
            status: '',
        },
        {
            id: 23,
            title: 'Text a friend I haven`t talked to in a long time',
            date: 'Oct, 10 2019',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-20.jpeg',
            status: '',
        },
        {
            id: 24,
            title: 'Organize pantry',
            date: 'Feb, 24 2020',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-10.jpeg',
            status: '',
        },
        {
            id: 25,
            title: 'Buy a new house decoration',
            date: 'Mar, 25 2020',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-9.jpeg',
            status: '',
        },
        {
            id: 26,
            title: 'Plan a vacation I`ve always wanted to take',
            date: 'Mar, 30 2020',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-4.jpeg',
            status: '',
        },
        {
            id: 27,
            title: 'Clean out car',
            date: 'Apr, 3 2020',
            description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            descriptionText:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar feugiat consequat. Duis lacus nibh, sagittis id varius vel, aliquet non augue. Vivamus sem ante, ultrices at ex a, rhoncus ullamcorper tellus. Nunc iaculis eu ligula ac consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum mattis urna neque, eget posuere lorem tempus non. Suspendisse ac turpis dictum, convallis est ut, posuere sem. Etiam imperdiet aliquam risus, eu commodo urna vestibulum at. Suspendisse malesuada lorem eu sodales aliquam.',
            tag: '',
            priority: '',
            assignee: 'John Smith',
            path: 'profile-3.jpeg',
            status: '',
        },
    ];
    filteredTasks: any = [];
    pagedTasks: any = [];
    searchTask = '';
    selectedTask: any = this.defaultParams;
    isPriorityMenu: any = null;
    isTagMenu: any = null;

    pager = {
        currentPage: 1,
        totalPages: 0,
        pageSize: 10,
        startIndex: 0,
        endIndex: 0,
    };

    editorOptions = {
        toolbar: [[{ header: [1, 2, false] }], ['bold', 'italic', 'underline', 'link'], [{ list: 'ordered' }, { list: 'bullet' }], ['clean']],
    };

    ngOnInit() {
        this.searchTasks();
    }

    initForm() {
        this.params = this.fb.group({
            id: [null],
            title: ['', Validators.required],
            description: [''],
            descriptionText: [''],
            assignee: [''],
            path: [''],
            tag: [''],
            priority: ['low'],
        });
    }

    searchTasks(isResetPage = true) {
        if (isResetPage) {
            this.pager.currentPage = 1;
        }
        let res;
        if (this.selectedTab === 'complete' || this.selectedTab === 'important' || this.selectedTab === 'trash') {
            res = this.allTasks.filter((d) => d.status === this.selectedTab);
        } else {
            res = this.allTasks.filter((d) => d.status != 'trash');
        }

        if (this.selectedTab === 'team' || this.selectedTab === 'update') {
            res = res.filter((d) => d.tag === this.selectedTab);
        } else if (this.selectedTab === 'high' || this.selectedTab === 'medium' || this.selectedTab === 'low') {
            res = res.filter((d) => d.priority === this.selectedTab);
        }
        this.filteredTasks = res.filter((d) => d.title?.toLowerCase().includes(this.searchTask));
        this.getPager();
    }

    getPager() {
        setTimeout(() => {
            if (this.filteredTasks.length) {
                this.pager.totalPages = this.pager.pageSize < 1 ? 1 : Math.ceil(this.filteredTasks.length / this.pager.pageSize);
                if (this.pager.currentPage > this.pager.totalPages) {
                    this.pager.currentPage = 1;
                }
                this.pager.startIndex = (this.pager.currentPage - 1) * this.pager.pageSize;
                this.pager.endIndex = Math.min(this.pager.startIndex + this.pager.pageSize - 1, this.filteredTasks.length - 1);
                this.pagedTasks = this.filteredTasks.slice(this.pager.startIndex, this.pager.endIndex + 1);
            } else {
                this.pagedTasks = [];
                this.pager.startIndex = -1;
                this.pager.endIndex = -1;
            }
        });
    }

    setPriority(task: any, name: string = '') {
        let item = this.filteredTasks.find((d: { id: any }) => d.id === task.id);
        item.priority = name;
        this.searchTasks(false);
    }

    setTag(task: any, name: string = '') {
        let item = this.filteredTasks.find((d: { id: any }) => d.id === task.id);
        item.tag = name;
        this.searchTasks(false);
    }

    tabChanged(type: any = null) {
        this.selectedTab = type;
        this.searchTasks();
        this.isShowTaskMenu = false;
    }

    taskComplete(task: any = null) {
        let item = this.filteredTasks.find((d: { id: any }) => d.id === task.id);
        item.status = item.status === 'complete' ? '' : 'complete';
        this.searchTasks(false);
    }

    setImportant(task: any = null) {
        let item = this.filteredTasks.find((d: { id: any }) => d.id === task.id);
        item.status = item.status === 'important' ? '' : 'important';
        this.searchTasks(false);
    }

    viewTask(item: any = null) {
        this.selectedTask = item;
        setTimeout(() => {
            this.viewTaskModal.open();
        });
    }

    addEditTask(task: any = null) {
        this.isShowTaskMenu = false;

        this.addTaskModal.open();
        this.initForm();

        if (task) {
            this.params.setValue({
                id: task.id,
                title: task.title,
                description: task.description,
                descriptionText: task.descriptionText,
                assignee: task.assignee,
                path: task.path,
                tag: task.tag,
                priority: task.priority,
            });
        }
    }

    deleteTask(task: any, type: string = '') {
        if (type === 'delete') {
            let currtask = this.allTasks.find((d: any) => d.id === task.id);
            currtask!.status = 'trash';
        }
        if (type === 'deletePermanent') {
            this.allTasks = this.allTasks.filter((d: any) => d.id != task.id);
        } else if (type === 'restore') {
            let currtask = this.allTasks.find((d: any) => d.id === task.id);
            currtask!.status = '';
        }
        this.searchTasks(false);
    }

    saveTask() {
        if (!this.params.value.title) {
            this.showMessage('Title is required.', 'error');
            return;
        }

        if (this.params.value.id) {
            //update task
            this.allTasks = this.allTasks.map((d: any) => {
                if (d.id === this.params.value.id) {
                    d = {...d, ...this.params.value};
                    d.descriptionText = this.params.value.descriptionText; //this.quillEditorObj.getText();
                }
                return d;
            });
            this.searchTasks();
        } else {
            //add task
            const maxid = this.allTasks.length ? this.allTasks.reduce((max, obj) => (obj.id > max ? obj.id : max), this.allTasks[0].id) : 0;

            const today = new Date();
            const dd = String(today.getDate()).padStart(2, '0');
            const mm = String(today.getMonth());
            const yyyy = today.getFullYear();
            const monthNames: any = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

            let task = this.params.value;
            task.id = maxid + 1;
            task.descriptionText = this.params.value.descriptionText; //this.quillEditorObj.getText();
            task.date = monthNames[mm] + ', ' + dd + ' ' + yyyy;

            this.allTasks.splice(0, 0, task);
            this.searchTasks();
        }

        this.showMessage('Task has been saved successfully.');
        this.addTaskModal.close();
    }

    showMessage(msg = '', type = 'success') {
        const toast: any = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 3000,
            customClass: { container: 'toast' },
        });
        toast.fire({
            icon: type,
            title: msg,
            padding: '10px 20px',
        });
    }

    setDiscriptionText(event: any) {
        this.params.patchValue({ descriptionText: event.text });
    }

    getTasksLength(type: string) {
        return this.allTasks.filter((task) => task.status == type).length;
    }
}
