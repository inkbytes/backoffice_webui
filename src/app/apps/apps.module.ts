
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// modal
import { ModalModule } from 'angular-custom-modal';

// sortable
import { SortablejsModule } from '@dustfoundation/ngx-sortablejs';

// headlessui
import { MenuModule } from 'headlessui-angular';

// perfect-scrollbar
import { NgScrollbarModule } from 'ngx-scrollbar';

// quill editor
import { QuillModule } from 'ngx-quill';

// fullcalendar
import { FullCalendarModule } from '@fullcalendar/angular';

// tippy
import { NgxTippyModule } from 'ngx-tippy-wrapper';

// datatable
import { DataTableModule } from '@bhplugin/ng-datatable';

// icon
import { IconModule } from 'src/app/shared/icon/icon.module';

import { ScrumboardComponent } from './scrumboard';
import { ContactsComponent } from './contacts';
import { NotesComponent } from './notes';
import { LogsComponent } from './logs/logs';
import { TodolistComponent } from './todolist';
import { SessionsComponent } from './sessions/sessions';
import { InvoicePreviewComponent } from './invoice/preview';
import { InvoiceAddComponent } from './invoice/add';
import { InvoiceEditComponent } from './invoice/edit';
import { CalendarComponent } from './calendar';
import { ChatComponent } from './chat';
import { MailboxComponent } from './mailbox';
import { InvoiceListComponent } from './invoice/list';
import { ViewerComponent } from "./logs/viewer/viewer.component";
import { MailToolbarComponent } from "../components/focused-toolbar/focused-toolbar.component";
import { ActionButtonComponent } from "../components/action-button/action-button.component";

import { ComponentsModule } from '@app/components/components.module';
import { FilesViewerComponent } from './file_explorer/file_explorer';
import { FileFolderViewComponent } from '@app/components/file-manager/file-folder-view/file-folder-view.component';
import { FolderCardComponent } from '@app/components/file-manager/folder-card/folder-card.component';
import { FileManagerModule } from '@app/components/file-manager/file-manager.module';


const routes: Routes = [
    { path: 'apps/chat', component: ChatComponent, title: 'Chat | InkBytes Nextgen News' },
    { path: 'apps/mailbox', component: MailboxComponent, title: 'Mailbox | InkBytes Nextgen News' },
    { path: 'apps/scrumboard', component: ScrumboardComponent, title: 'Scrumboard | InkBytes Nextgen News' },
    { path: 'apps/contacts', component: ContactsComponent, title: 'Contacts | InkBytes Nextgen News' },
    { path: 'apps/notes', component: NotesComponent, title: 'Notes | InkBytes Nextgen News' },
     { path:'apps/logs', component: LogsComponent, title: 'Logs | InkBytes Nextgen News' },
    { path: 'apps/todolist', component: TodolistComponent, title: 'Todolist | InkBytes Nextgen News' },
    { path: 'apps/file_explorer/file_explorer', component: FilesViewerComponent, title: 'Files | InkBytes Nextgen News' },
    { path: 'apps/sessions/sessions', component: SessionsComponent, title: 'Sessions | InkBytes Nextgen News' },
    { path: 'apps/invoice/list', component: InvoiceListComponent, title: 'Invoice List | InkBytes Nextgen News' },
    { path: 'apps/invoice/preview', component: InvoicePreviewComponent, title: 'Invoice Preview | InkBytes Nextgen News' },
    { path: 'apps/invoice/add', component: InvoiceAddComponent, title: 'Invoice Add | InkBytes Nextgen News' },
    { path: 'apps/invoice/edit', component: InvoiceEditComponent, title: 'Invoice Edit | InkBytes Nextgen News' },
    { path: 'apps/calendar', component: CalendarComponent, title: 'Calendar | InkBytes Nextgen News' },
];

@NgModule({
    declarations: [
        ChatComponent,
        ScrumboardComponent,
        ContactsComponent,
        NotesComponent,
        LogsComponent,
        TodolistComponent,
        SessionsComponent,
        InvoiceListComponent,
        InvoicePreviewComponent,
        InvoiceAddComponent,
        InvoiceEditComponent,
        CalendarComponent,
        FilesViewerComponent,
        MailboxComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        SortablejsModule,
        ComponentsModule,
        MenuModule,
        NgScrollbarModule.withConfig({
            visibility: 'hover',
            appearance: 'standard',
        }),
        QuillModule.forRoot(),
        FullCalendarModule,
        NgxTippyModule,
        DataTableModule,
        IconModule,
        FileManagerModule,
        ViewerComponent,
        MailToolbarComponent,

        ActionButtonComponent
    ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppsModule {}
