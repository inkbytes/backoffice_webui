import { SvgLoaderService } from './../../apis/svg-loader.service';
import { CommonModule } from '@angular/common';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { NgxTippyModule } from 'ngx-tippy-wrapper';
@Component({
  selector: 'app-action-button',
  templateUrl: './action-button.component.html',
  standalone:true,
  imports:[NgxTippyModule,CommonModule],
  styleUrls: ['./action-button.component.css']
})
export class ActionButtonComponent {
  @Input() icon: string;
  @Input() tooltip: string;
  @Output() clickEvent = new EventEmitter<void>();
  @Output() svgIcon:SafeHtml;
path: any;
content: any;
  constructor() {

  }

  onClick() {
    this.clickEvent.emit();
  }

}
