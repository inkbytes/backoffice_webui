import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// highlightjs
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';

// modal
import { ModalModule } from 'angular-custom-modal';

// counter
import { CountUpModule } from 'ngx-countup';

// lightbox
import { LightboxModule } from 'ngx-lightbox';

// headlessui
import { MenuModule } from 'headlessui-angular';

// icon
import { IconModule } from 'src/app/shared/icon/icon.module';

import { TabsComponent } from './tabs';
import { AccordionsComponent } from './accordions';
import { ModalsComponent } from './modals';
import { CardsComponent } from './cards';
import { CarouselComponent } from './carousel';
import { CountdownComponent } from './countdown';
import { CounterComponent } from './counter';
import { SweetalertComponent } from './sweetalert';
import { TimelineComponent } from './timeline';
import { NotificationsComponent } from './notifications';
import { MediaObjectComponent } from './media-object';
import { ListGroupComponent } from './list-group';
import { PricingTableComponent } from './pricing-table';
import { LightboxComponent } from './lightbox';
import { FileFolderViewComponent } from './file-manager/file-folder-view/file-folder-view.component';
import { FolderCardComponent } from './file-manager/folder-card/folder-card.component';
import { FileManagerModule } from './file-manager/file-manager.module';


const routes: Routes = [
    { path: 'components/tabs', component: TabsComponent, title: 'Tabs | InkBytes Nextgen News' },
    { path: 'components/accordions', component: AccordionsComponent, title: 'Accordions | InkBytes Nextgen News' },
    { path: 'components/modals', component: ModalsComponent, title: 'Modals | InkBytes Nextgen News' },
    { path: 'components/cards', component: CardsComponent, title: 'Cards | InkBytes Nextgen News' },
    { path: 'components/carousel', component: CarouselComponent, title: 'Carousel | InkBytes Nextgen News' },
    { path: 'components/countdown', component: CountdownComponent, title: 'Countdown | InkBytes Nextgen News' },
    { path: 'components/counter', component: CounterComponent, title: 'Counter | InkBytes Nextgen News' },
    { path: 'components/sweetalert', component: SweetalertComponent, title: 'Sweetalert | InkBytes Nextgen News' },
    { path: 'components/timeline', component: TimelineComponent, title: 'Timeline | InkBytes Nextgen News' },
    { path: 'components/notifications', component: NotificationsComponent, title: 'Notifications | InkBytes Nextgen News' },
    { path: 'components/media-object', component: MediaObjectComponent, title: 'Media Object | InkBytes Nextgen News' },
    { path: 'components/list-group', component: ListGroupComponent, title: 'List Group | InkBytes Nextgen News' },
    { path: 'components/pricing-table', component: PricingTableComponent, title: 'Pricing Table | InkBytes Nextgen News' },
    { path: 'components/lightbox', component: LightboxComponent, title: 'Lightbox | InkBytes Nextgen News' },
];
@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HighlightModule,
        ModalModule,
        CountUpModule,
        LightboxModule,
        MenuModule,
        IconModule,
    ],
    declarations: [
        TabsComponent,
        AccordionsComponent,
        ModalsComponent,
        CardsComponent,
        CarouselComponent,
        CountdownComponent,
        CounterComponent,
        SweetalertComponent,
        TimelineComponent,
        NotificationsComponent,
        MediaObjectComponent,
        ListGroupComponent,
        PricingTableComponent,
        FolderCardComponent,
        FileFolderViewComponent,
        LightboxComponent,
    ],
    providers: [
        {
            provide: HIGHLIGHT_OPTIONS,
            useValue: {
                coreLibraryLoader: () => import('highlight.js/lib/core'),
                languages: {
                    json: () => import('highlight.js/lib/languages/json'),
                    typescript: () => import('highlight.js/lib/languages/typescript'),
                    xml: () => import('highlight.js/lib/languages/xml'),
                },
            },
        },
    ],
    exports:[
        FileFolderViewComponent,
        FolderCardComponent
    ]
})
export class ComponentsModule {}
