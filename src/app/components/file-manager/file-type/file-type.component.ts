// src/app/icon-filetype/icon-filetype.component.ts
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-filetype',
  templateUrl: './file-type.component.html',
  styleUrls: ['./file-type.component.css']
})
export class FileTypeComponent implements OnInit {
  @Input() filename: string;
  fileType: string;

  ngOnInit(): void {
    this.fileType = this.getFileType(this.filename);
  }
  getFileType(filename?: string): string {
    const extension = (filename ?? '').split('.').pop()?.toLowerCase();
    switch (extension) {
      case 'pdf':
        return 'pdf';
      case 'doc':
      case 'docx':
        return 'word';
      case 'xls':
      case 'xlsx':
        return 'excel';
      case 'jpg':
      case 'jpeg':
      case 'png':
      case 'gif':
        return 'image';
      // Add more cases as needed
      default:
        return 'file';
    }
  }
}

