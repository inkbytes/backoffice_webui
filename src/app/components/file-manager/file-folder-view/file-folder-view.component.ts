import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';



// file-folder-view.component.ts

export interface FileFolder {
  name: string;
  type: 'file' | 'folder';
  children?: FileFolder[]; // Only for folders
}

@Component({
    selector: 'app-file-folder-view',
    templateUrl: './file-folder-view.component.html',
    styleUrls: ['./file-folder-view.component.css']
})
export class FileFolderViewComponent implements OnInit {
    fileFolders: FileFolder[] = [
        {
            name: 'Folder 1',
            type: 'folder',
            children: [
                { name: 'File 1.txt', type: 'file' },
                { name: 'File 2.txt', type: 'file' }
            ]
        },
        { name: 'File 3.txt', type: 'file' }
    ];

    constructor() {
        console.log(this.fileFolders)
     }

    ngOnInit(): void {
    }
}

