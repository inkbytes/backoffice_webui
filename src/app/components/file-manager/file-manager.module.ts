
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileSystemService } from './file-system.service';
import { FileExplorerComponent } from './file-explorer/file-explorer.component';
import { IconModule } from '@app/shared/icon/icon.module';
import { FileTypeComponent } from './file-type/file-type.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';





@NgModule({
  declarations: [FileExplorerComponent, FileTypeComponent],

  imports: [
    CommonModule,
    IconModule,
    FontAwesomeModule
  ],
  //providers:[FileSystemService],
  exports: [FileExplorerComponent,FileTypeComponent]
})
export class FileManagerModule { }
