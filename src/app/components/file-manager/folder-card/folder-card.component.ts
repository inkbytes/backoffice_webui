
import {  ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

export interface Folder {
id: number;
    tag?: 'personal' | 'work' | 'social' | 'important';
  thumb?: string;
  user?: string;
  date?: string;
  title: string;
  description: string;
  isFav?: boolean;
}
@Component({
  selector: 'folder-card',
  templateUrl: './folder-card.component.html',
  styleUrls: ['./folder-card.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush

})

export class FolderCardComponent implements OnInit{
    @Input() folder: Folder ;
    @Output() setEmitFav = new EventEmitter<Folder>();

    ngOnInit(): void {

    }
    setFav(folder:Folder): void{
        this.folder.isFav = !this.folder.isFav;
        this.setEmitFav.emit(this.folder);
    }
}
