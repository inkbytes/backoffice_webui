import { Output } from '@angular/core';
// src/app/file-explorer/file-explorer.component.ts
import { Component, OnInit } from '@angular/core';
import { FileElement } from '../model/file-element';
import { FileSystemService } from '../file-system.service';
import { DoFilesService } from '@app/apis/digitalocean.service';


@Component({
  selector: 'app-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.css']
})
export class FileExplorerComponent implements OnInit {
 fileElements: FileElement[];
    files: any;
  constructor(private fileSystemService: DoFilesService) {

  }

  ngOnInit(): void {
    this.fileSystemService.getFiles().subscribe(elements => {
      this.fileElements = elements;


    });
   //this.fileSystemService.listFiles()


  }
  /*listFiles() {
    this.fileSystemService.listFiles().then(data => {
      console.log(data);
      this.files = data.Contents;
    }).catch(error => {
      console.error("Error listing files: ", error);
    });
  }*/
toggleFolder(element: FileElement): void {
    if (element.type === 'folder') {
      element.expanded = !element.expanded;
    }
  }
  // Add methods for handling user interactions like adding, removing elements...
}
