export class FileElement {
  id?: string; // Unique identifier for each element
  name: string; // Name of the file or folder
  type: 'folder' | 'file'; // Type of the element
  parent?: FileElement; // Reference to the parent element, if any
  children?: FileElement[]; // Only for folders, to hold child elements
 expanded?: boolean | undefined;

  constructor(
    name: string,
    type: 'folder' | 'file',
    parent?: FileElement,
    children?: FileElement[],
    id?: string,
    expanded?: boolean // Add this line
  ) {
    this.name = name;
    this.type = type;
    this.parent = parent;
    this.children = children;
    this.id = id;
    this.expanded = expanded; // Add this line
  }
}
