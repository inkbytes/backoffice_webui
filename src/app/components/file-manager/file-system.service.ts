// src/app/services/file-system.service.ts
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FileElement } from './model/file-element';
import { DoFilesService } from '@app/apis/digitalocean.service';

@Injectable({
  providedIn: 'root'
})
export class FileSystemService {
  private fileElements: BehaviorSubject<FileElement[]> = new BehaviorSubject<FileElement[]>([]);

  constructor(private doService: DoFilesService) {
    this.fetchFiles();
  }

  private fetchFiles(): void {
    this.doService.getFiles().subscribe(files => {
      this.fileElements.next(files);
    }, error => {
      console.error('Failed to fetch files:', error);
      // Handle errors appropriately in your application context
    });
  }

  getFileElements(): Observable<FileElement[]> {
    return this.fileElements.asObservable();
  }

  // Implement the upload logic here or in the dedicated service as per your application architecture
  uploadFile(file: File): void {
    // Implementation depends on where you wish to handle the actual upload logic
    console.log('Upload file logic goes here.');
  }

  // Uncomment and implement the removeFileElement if needed
  removeFileElement(id: string): void {
    const updatedElements = this.fileElements.value.filter(element => element.id !== id);
    this.fileElements.next(updatedElements);
  }

  // Add more methods as needed...
}
