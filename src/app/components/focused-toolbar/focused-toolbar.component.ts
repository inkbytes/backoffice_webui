import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { IconModule } from "../../shared/icon/icon.module";
import { ActionButtonComponent } from '../action-button/action-button.component';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
    selector: 'focused-toolbar',
    templateUrl: './focused-toolbar.component.html',
    standalone: true,
    styleUrls: ['./focused-toolbar.component.css'],
    imports: [CommonModule, NgScrollbarModule, IconModule,ActionButtonComponent]
})
export class MailToolbarComponent {
  isShowMailMenu = true;
  selectedTab = 'inbox';
isShowDropdown: any;
isEdit: any;
content: any;

  constructor(private sanitizer: DomSanitizer) { }
    getSafeSvgContent(svgContent: string) {
  return this.sanitizer.bypassSecurityTrustHtml(svgContent);
}
  openMail(action: string, mail: any): void {
    // Implement the action to open a new message or specific mail
  }

  tabChanged(tabName: string): void {
    this.selectedTab = tabName;
    // Implement additional logic as needed when changing tabs
  }

  getMailsLength(tab: string): number {
    // Return the number of mails in the specified tab
    // Placeholder implementation
    return 0;
  }
}
