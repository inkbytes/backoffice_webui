import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageQueueService {
  private baseUrl = 'http://your-backend-service/api';  // URL to web API

  constructor(private http: HttpClient) { }

  // Send message to the backend, which will then send it to RabbitMQ
  sendMessage(message: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/messages/send`, message);
  }

  // Fetch messages from the backend, which pulls them from RabbitMQ
  receiveMessages(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/messages/receive`);
  }
}

