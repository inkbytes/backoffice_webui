
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Session } from 'app/models/sessions/session';
import { SessionsAdapter } from 'app/models/sessions/sessions.adapter';
import { BehaviorSubject, Observable, catchError, map, of, switchMap, take, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScrappingSessionsService {
    private _session: BehaviorSubject<Session | null> = new BehaviorSubject(null);
    private _sessions: BehaviorSubject<Session[] | null> = new BehaviorSubject(null);
    private strapiUrl = 'https://cms.kloudsix.io/api/scrapesessions'; // Adjust with your actual Strapi URL
    private adapter: SessionsAdapter=new SessionsAdapter()
    sessions$: Observable<Session[]>;
    sessions: Session[];
    token="4521fc0c8d29538fbe79f028539e487b70dab893fdb338f48ad4ca9e8bb200aa595418e9831c6e572e69c7040754580802fa9406140b640b9a6fd5a1ddeaf12699a8d0f672749439bd28eaa2e5c8bddb24157dd2dc32f174c237c43a579645796a140236e6637bf6a7e92ce8d13040a232e8af1220e80e10b7149d55985b221a";
constructor(

        private http: HttpClient,

    ) {
        this.sessions$ = this.fetchSessions();
        this.sessions$.subscribe((sessions: Session[]) => {
            this._sessions.next(sessions);
            this.sessions= sessions;
        });
    }

    private transformResponse(response: any): Session[] {
        response.data.meta=response.data.meta;
        return response.data.map((item) => this.adapter.adapt(item));
    }
    get session$(): Observable<Session> {
        return this._session.asObservable();
    }
    fetchSessions(): Observable<Session[]> {
          return this.http.get<Session>(this.strapiUrl).pipe(
            map((response) => this.transformResponse(response)),
            catchError((error) => throwError(() => new Error(error)))
        );
    }
    // Get a session by ID
    getSessionById(id: number): Observable<Session> {
        return this.sessions$.pipe(
            take(1),
            map((sessions) => {
                const session = sessions.find(item => item.id === id) || null;
                console.log(sessions);
                console.log(session);
                this._session.next(session);
                return session;
            }),
            switchMap((session) => {
                if (!session) {
                    return throwError(() => new Error('Could not found session with id of ' + id + '!'));
                }
                return of(session);
            }),
        );
    }

    // Create a session
    createSession(sessionSource: Session): Observable<Session> {
        return this.http.post<Session>(this.strapiUrl, sessionSource).pipe(
            map((response) => this.adapter.adapt(response)), // Adjust if needed
            catchError((error) => throwError(() => new Error(error)))
        );
    }

    // Update a session
    updateSession(id: string, sessionSource: Session): Observable<Session> {
        const url = `${this.strapiUrl}/${id}`;
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.token}`,
            // Add other headers if needed
        });

        return this.http.put<Session>(url, { "data": sessionSource }, { headers }).pipe(
            map((response) => this.adapter.adapt(response)), // Adjust if needed
            catchError((error) => throwError(() => new Error(error)))
        );
    }

    // Delete a session
    deleteSession(id: string): Observable<any> {
        const url = `${this.strapiUrl}/${id}`;
        return this.http.delete(url).pipe(
            catchError((error) => throwError(() => new Error(error)))
        );
    }
    }
