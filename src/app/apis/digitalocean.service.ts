import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FileElement } from '@models/file-element'; // Adjust the import path as necessary

@Injectable({
  providedIn: 'root'
})
export class DoFilesService {
  private baseUrl = 'http://localhost:3000/dofiles'; // Adjust the URL as needed

  constructor(private http: HttpClient) { }

  getFiles(): Observable<FileElement[]> {
    // Note: HttpClient assumes JSON response type by default
    return this.http.get<FileElement[]>(this.baseUrl);
  }
}

