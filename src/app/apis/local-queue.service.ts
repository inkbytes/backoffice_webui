import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalQueueService {
  private localMessageQueue: any[] = [];


  constructor() {
    this.loadLocalQueueFromDisk();
  }

  // Save the local queue to local storage
  saveLocalQueueToDisk(): void {
    localStorage.setItem('localMessageQueue', JSON.stringify(this.localMessageQueue));
  }

  // Load the local queue from local storage
  loadLocalQueueFromDisk(): void {
    const data = localStorage.getItem('localMessageQueue');
    if (data) {
      this.localMessageQueue = JSON.parse(data);
    }
  }

  // Add a message to the local queue
  queueMessage(message: any): void {
    this.localMessageQueue.push(message);
    this.saveLocalQueueToDisk();
  }

  // Get all messages from the local queue
  getLocalMessages(): any[] {
    return this.localMessageQueue;
  }

  // Clear the local message queue
  clearLocalQueue(): void {
    this.localMessageQueue = [];
    this.saveLocalQueueToDisk();
  }
}
