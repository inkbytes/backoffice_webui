import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SvgLoaderService {

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  loadSvg(svgUrl: string): Observable<SafeHtml> {
    return this.http.get(svgUrl, { responseType: 'text' })
      .pipe(
        map(svgContent => this.sanitizer.bypassSecurityTrustHtml(svgContent))
      );
  }
}






