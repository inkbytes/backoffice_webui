import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  private socket: WebSocket;
  private messageSubject: Subject<string> = new Subject<string>();
  public messages: Observable<string> = this.messageSubject.asObservable();

  constructor() {}

  public connect(url: string): void {
    this.socket = new WebSocket(url);

    this.socket.onmessage = (event) => {

      this.messageSubject.next(event.data);
    };

    this.socket.onopen = () => {
      console.log('WebSocket connection established');

    };

    this.socket.onerror = (event) => {
      console.error('WebSocket error observed:', event);
    };

    this.socket.onclose = () => {
      console.log('WebSocket connection closed');
    };
  }
  public isConnected(): boolean {
  return this.socket && this.socket.readyState === WebSocket.OPEN;
}
    public sendMessage(message: string): void {
      if (this.socket && this.socket.readyState === WebSocket.OPEN) {
    this.socket.send(message);
    // this.socket.send('start_scrape'); // Automatically start scrape on connect, adjust as needed
      } else {
    console.error('WebSocket is not connected.');
      }
    }
  public disconnect(): void {
    if (this.socket) {
      this.socket.close();
    }
  }
}
