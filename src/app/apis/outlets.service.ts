import { Injectable, SkipSelf } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, catchError, map, of, switchMap, take, throwError } from 'rxjs';
import { OutletSourceAdapter } from 'app/models/outlet/outlet-source.adapter';
import { OutletSource } from 'app/models/outlet/outlet-source';
import { Contact } from 'app/layout/common/quick-chat/quick-chat.types';
import { Country, Tag } from 'app/modules/admin/apps/outlets/outlets.types';

@Injectable({
    providedIn: 'root',
})
export class OutletSourceService {
    // Private
    private _outlet: BehaviorSubject<OutletSource | null> = new BehaviorSubject(null);
    private _outlets: BehaviorSubject<OutletSource[] | null> = new BehaviorSubject(null);
    private _countries: BehaviorSubject<Country[] | null> = new BehaviorSubject(null);
    private _tags: BehaviorSubject<Tag[] | null> = new BehaviorSubject(null);
    private strapiUrl = 'https://cms.kloudsix.io/api/outletssources'; // Adjust with your actual Strapi URL
    private adapter: OutletSourceAdapter=new OutletSourceAdapter();
    outlets$: Observable<OutletSource[]>;
    outlets: OutletSource[];

    constructor(

        private http: HttpClient,

    ) {
        this.outlets$ = this.fetchOutletSources();
        this.outlets$.subscribe((outlets: OutletSource[]) => {
            this._outlets.next(outlets);
            this.outlets= outlets;
        });
    }

    private fetchOutletSources(): Observable<OutletSource[]> {
        return this.http.get<OutletSource>(this.strapiUrl).pipe(
            map((response) => this.transformResponse(response)),
            catchError((error) => throwError(() => new Error(error)))
        );
    }

    private transformResponse(response: any): OutletSource[] {
        return response.data.map((item) => this.adapter.adapt(item));
    }
        /**
     * Getter for contact
     */
    get outlet$(): Observable<OutletSource>
    {
        return this._outlet.asObservable();
    }
    getOutletById(id: number): Observable<OutletSource>
    {
        return this.outlets$.pipe(
            take(1),
            map((outlets) =>
            {
                // Find the contact

                const outlet = outlets.find(item => item.id == id) || null;
                console.log(outlets)
                console.log(outlet);
                // Update the contact
                this._outlet.next(outlet);

                // Return the contact
                return outlet;
            }),
            switchMap((outlet) =>
            {
                if ( !outlet )
                {
                    return throwError('Could not found outlet with id of ' + id + '!');
                }

                return of(outlet);
            }),
        );
    }
    createOutletSource(outletSource: OutletSource): Observable<OutletSource> {
        return this.http.post<OutletSource>(this.strapiUrl, outletSource).pipe(
            map((response) => this.adapter.adapt(response)), // Adjust if needed
            catchError((error) => throwError(() => new Error(error)))
        );
    }
    token="4521fc0c8d29538fbe79f028539e487b70dab893fdb338f48ad4ca9e8bb200aa595418e9831c6e572e69c7040754580802fa9406140b640b9a6fd5a1ddeaf12699a8d0f672749439bd28eaa2e5c8bddb24157dd2dc32f174c237c43a579645796a140236e6637bf6a7e92ce8d13040a232e8af1220e80e10b7149d55985b221a";

    updateOutletSource(id: string, outletSource: OutletSource): Observable<OutletSource> {
        const url = `${this.strapiUrl}/${id}`;
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.token}`,
            // Add other headers if needed
        });

        return this.http.put<OutletSource>(url, {"data":outletSource}).pipe(
            map((response) => this.adapter.adapt(response)), // Adjust if needed
            catchError((error) => throwError(() => new Error(error)))
        );
    }

    deleteOutletSource(id: string): Observable<any> {
        const url = `${this.strapiUrl}/${id}`;
        return this.http.delete(url).pipe(
            catchError((error) => throwError(() => new Error(error)))
        );
    }
}

