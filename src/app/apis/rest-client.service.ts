import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class RestClientService {
  private baseUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getBaseUrl(): string {
    return this.baseUrl;
  }

  setBaseUrl(value: string): void {
    this.baseUrl = value;
  }

  sendApiRequest(
    method: string,
    endpoint: string,
    data?: any,
    headers?: HttpHeaders
  ): Observable<any> {
    return this._sendRequest(method, endpoint, data, headers).pipe(
      catchError((error) => {
        console.error(`Error sending ${method} request to ${this._constructFullUrl(endpoint)}: ${error}`);
        return throwError(error);
      })
    );
  }

  private _sendRequest(
    method: string,
    endpoint: string,
    data?: any,
    headers?: HttpHeaders
  ): Observable<HttpResponse<any>> {
    const url = this._constructFullUrl(endpoint);
    const requestOptions = {
      headers: headers || new HttpHeaders(),
      observe: 'response' as const,
    };

    return this.http.request(method, url, { ...requestOptions, body: data }).pipe(
      catchError((error) => {
        console.error(`Error sending ${method} request to ${url}: ${error}`);
        return throwError(error);
      })
    );
  }

  private _constructFullUrl(endpoint: string): string {
    return `${this.baseUrl}/${endpoint}`;
  }
}
