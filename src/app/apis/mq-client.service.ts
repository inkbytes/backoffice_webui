import { Client, IMessage } from '@stomp/stompjs';
import { environment } from '../../environments/environment.prod';

export class RabbitMQService {
    private client: Client;
    private queueName: string;

    /**
     * Initializes the RabbitMQService with specific configurations.
     *
     * @param heartbeatIncoming - The interval in milliseconds for incoming heartbeats.
     * @param heartbeatOutgoing - The interval in milliseconds for outgoing heartbeats.
     * @param reconnectDelay - The delay in milliseconds before attempting to reconnect.
     */
    constructor(
        heartbeatIncoming: number = 14000,
        heartbeatOutgoing: number = 14000,
        reconnectDelay: number = 5000
    ) {
        const { brokerURL, queueName, login, passcode, vhost } = environment.rabbitmq;
        this.queueName = queueName;
        this.client = new Client({
            brokerURL,
            connectHeaders: {
                login,
                passcode,
                vhost,
            },
            debug: (str) => {
                console.log(str); // Debugging output, consider toggling based on environment
            },
            reconnectDelay,
            heartbeatIncoming,
            heartbeatOutgoing,
        });

        this.client.onConnect = this.onConnect;
        this.client.onStompError = this.onStompError;
        this.client.onDisconnect = this.onDisconnect;
    }

    /**
     * Connects the client to the RabbitMQ server.
     */
    connect(): void {
        this.client.activate();
    }

    /**
     * Disconnects the client from the RabbitMQ server.
     */
    disconnect(): void {
        this.client.deactivate();
    }
    getClient(): Client {
        return this.client;
    }
    /**
     * Sends a message to the queue.
     *
     * @param message - The message to be sent.
     */
    sendMessage(message: string): void {
        if (this.client.active) {
            this.client.publish({
                destination: this.queueName,
                body: message
            });
            console.log(`Message sent: ${message}`);
        } else {
            console.error('Cannot send message. Client is not connected.');
        }
    }
    onMessageReceived(callback: (msg: IMessage) => void): void {
        this.client.subscribe(this.queueName, callback);
    }
    // Private method to handle connection logic
    private onConnect = () => {
        console.log('Connected to RabbitMQ-Web-Stomp');
        this.subscribeToQueue();
        this.client.publish({ destination: this.queueName, body: 'First Message' });
    }

    // Private method to handle disconnection logic
    private onDisconnect = () => {
        console.log('Disconnected from RabbitMQ-Web-Stomp');
        // Implement your reconnection logic or clean-up here
    }

    // Private method to handle STOMP errors
    private onStompError = (frame) => {
        console.error(`Broker reported error: ${frame.headers['message']}`);
        console.error(`Additional details: ${frame.body}`);
    }

    // Private method to subscribe to the queue
    private subscribeToQueue() {
        console.log(`Subscribing to queue: ${this.queueName}`);
        this.client.subscribe(this.queueName, (message: IMessage) => {
            console.log(`Received message: ${message.body}`);
            // Handle message here
        });
    }
}
