import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// icon
import { IconModule } from 'src/app/shared/icon/icon.module';

import { ContactUsBoxedComponent } from './contact-us-boxed';
import { ContactUsCoverComponent } from './contact-us-cover';
import { ComingSoonBoxedComponent } from './coming-soon-boxed';
import { ComingSoonCoverComponent } from './coming-soon-cover';
import { Error404Component } from './error404';
import { Error500Component } from './error500';
import { Error503Component } from './error503';
import { MaintenenceComponent } from './maintenence';

// headlessui
import { MenuModule } from 'headlessui-angular';
import { FaqComponent } from './faq';
import { KnowledgeBaseComponent } from './knowledge-base';
import { ModalModule } from 'angular-custom-modal';

const routes: Routes = [
    { path: 'pages/knowledge-base', component: KnowledgeBaseComponent, title: 'Knowledge Base | InkBytes Nextgen News' },
    { path: 'pages/faq', component: FaqComponent, title: 'FAQ | InkBytes Nextgen News' },
    { path: 'pages/contact-us-boxed', component: ContactUsBoxedComponent, title: 'Contact Us Boxed | InkBytes Nextgen News' },
    { path: 'pages/contact-us-cover', component: ContactUsCoverComponent, title: 'Contact Us Cover | InkBytes Nextgen News' },
    { path: 'pages/coming-soon-boxed', component: ComingSoonBoxedComponent, title: 'Coming Soon Boxed | InkBytes Nextgen News' },
    { path: 'pages/coming-soon-cover', component: ComingSoonCoverComponent, title: 'Coming Soon Cover | InkBytes Nextgen News' },
    { path: 'pages/error404', component: Error404Component, title: 'Error 404 | InkBytes Nextgen News' },
    { path: 'pages/error500', component: Error500Component, title: 'Error 500 | InkBytes Nextgen News' },
    { path: 'pages/error503', component: Error503Component, title: 'Error 503 | InkBytes Nextgen News' },
    { path: 'pages/maintenence', component: MaintenenceComponent, title: 'Maintenence | InkBytes Nextgen News' },
];
@NgModule({
    imports: [RouterModule.forChild(routes), CommonModule, MenuModule, IconModule,ModalModule],
    declarations: [
        KnowledgeBaseComponent,
        ContactUsBoxedComponent,
        FaqComponent,
        ContactUsCoverComponent,
        ComingSoonBoxedComponent,
        ComingSoonCoverComponent,
        Error404Component,
        Error500Component,
        Error503Component,
        MaintenenceComponent,
    ],
})
export class PagesModule {}
